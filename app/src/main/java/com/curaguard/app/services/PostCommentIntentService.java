package com.curaguard.app.services;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.curaguard.app.utils.StringUtils;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class PostCommentIntentService extends IntentService {

    public PostCommentIntentService() {
        super("PostCommentIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.i("Service", "Service Started");

        String data =intent.getStringExtra(StringUtils.COMMENT_JSON_OBJECT);
        Log.i("Web Service Data", data);

        HttpClient httpClient = new DefaultHttpClient();
        HttpPost post = new HttpPost("http://www.codespikestudios.com/fb_app_new/chat_parse.php");
        post.setHeader("content-type", "application/json; charset=UTF-8");

        try {
            JSONObject commentJsonObject = new JSONObject(data);
            StringEntity entity = new StringEntity(commentJsonObject.toString());
            post.setEntity(entity);

            HttpResponse resp = httpClient.execute(post);
            String respStr = EntityUtils.toString(resp.getEntity());

            Log.i("Service Response", respStr);
        } catch (JSONException e){
            e.printStackTrace();
        } catch (IOException e){
            e.printStackTrace();
        }
    }
}
