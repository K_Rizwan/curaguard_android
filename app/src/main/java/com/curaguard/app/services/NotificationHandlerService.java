package com.curaguard.app.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

import com.curaguard.app.MainActivity;
import com.curaguard.app.R;
import com.curaguard.app.utils.TaskNotificationDescription;

/**
 * Created by Kashif on 4/25/2015.
 */
public class NotificationHandlerService extends Service{

    @Override
    public IBinder onBind(Intent intent) { return null; }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        int id = intent.getIntExtra(getString(R.string.notification_id_key), 0);
        String message = intent.getStringExtra(getString(R.string.notification_message_key));
        String taskDescription = intent.getStringExtra(getString(R.string.notification_description_key));
        //sendNotification(id, message);
        displayNotification(id, message, taskDescription);
        return super.onStartCommand(intent, flags, startId);
    }

    /*private void sendNotification(int id, String message){
        NotificationDelivery notificationDelivery = new NotificationDelivery(true, true);
        notificationDelivery.sendNotification(this, id, message);
        stopSelf();
    }*/

    protected void displayNotification(int notificationID, String message, String taskDescription) {
        NotificationCompat.Builder  mBuilder = new NotificationCompat.Builder(this);

        mBuilder.setContentTitle("Cura Guard");
        mBuilder.setContentText(message);
        mBuilder.setSmallIcon(R.mipmap.ic_launcher);
        mBuilder.setAutoCancel(true);

        Intent resultIntent = null;
        if (taskDescription.equals(TaskNotificationDescription.NEW_COMMENT_ON_TASK))
            resultIntent = new Intent(this, MainActivity.class);
        else
            resultIntent = new Intent(this, MainActivity.class);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(MainActivity.class);

      /* Adds the Intent that starts the Activity to the top of the stack */
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        mBuilder.setContentIntent(resultPendingIntent);

        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

      /* notificationID allows you to update the notification later on. */
        mNotificationManager.notify(notificationID, mBuilder.build());
    }
}
