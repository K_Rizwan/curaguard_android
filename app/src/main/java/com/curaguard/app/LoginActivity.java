package com.curaguard.app;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.curaguard.app.R;
import com.curaguard.app.models.User;
import com.curaguard.app.utils.DatabaseUtils;
import com.curaguard.app.utils.PreferenceManager;
import com.curaguard.app.utils.ProgressDialogManager;
import com.curaguard.app.utils.StringUtils;
import com.curaguard.app.utils.ToastManager;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseRelation;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class LoginActivity extends Activity {

    private CallbackManager callbackManager;
    private PreferenceManager preferenceManager;
    private ProgressDialogManager progressDialogManager;
    private LoginManager loginManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        preferenceManager = new PreferenceManager(this);
        progressDialogManager = new ProgressDialogManager(this);

        if(preferenceManager.isUserAlreadyLoggedIn(PreferenceManager.USER_LOGGED_IN))
            startMainActivity();
        else {
            callbackManager = CallbackManager.Factory.create();
            loginManager = LoginManager.getInstance();
            loginManager.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {
                    progressDialogManager.showDialog("Please Wait");
                    Log.i("AccessToken", ""+loginResult.getAccessToken().getToken());
                    GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                Log.v("JsonObject", ""+object.toString());
                                new SaveUserPictureTask().execute(object);
                            }
                        });
                    Bundle parameters = new Bundle();
                    parameters.putString("fields", "id,name,friends,email,gender,birthday");
                    request.setParameters(parameters);
                    request.executeAsync();
                }

                @Override
                public void onCancel() {
                    Toast.makeText(LoginActivity.this, "User canceled the Facebook login", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(FacebookException exception) {
                    exception.printStackTrace();
                    ToastManager.ShowToast(LoginActivity.this, exception.getMessage());
                }
            });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    public void onLoginClicked(View v){
        List<String> permissions = Arrays.asList("user_friends", "public_profile", "email", "user_birthday");
        loginManager.logInWithReadPermissions(this, permissions);
    }

    private void onFacebookLoginDone(final JSONObject object){
        try{
            final String name = object.getString("name");
            final String picturePath = "https://graph.facebook.com/" + object.getString("id") + "/picture?type=large";
            ParseUser.logInInBackground(name, CuraGuardApplication.PARSE_PASSWORD, new LogInCallback() {
                public void done(ParseUser user, ParseException e) {
                    if (user != null) {
                        try {
                            ParseRelation<ParseObject> relation = user.getRelation("friends");
                            ArrayList<ParseUser> relationalUsers = fetchUserFriendsFromParse(object);
                            ParseUser currentUser = ParseUser.getCurrentUser();
                            for (ParseUser friendUser : relationalUsers){
                                relation.add(friendUser);
                                ParseObject followedFriendObject = new ParseObject(DatabaseUtils.TABLE_FOLLOWED_FRIENDS);
                                followedFriendObject.put(DatabaseUtils.FOLLOWED_FRIENDS_CURRENT_USER, currentUser);
                                followedFriendObject.put(DatabaseUtils.FOLLOWED_FRIENDS_FRIEND_USER, friendUser);
                                followedFriendObject.put(DatabaseUtils.FOLLOWED_FRIENDS_IS_FOLLOWED, true);
                                followedFriendObject.save();
                            }
                            user.save();
                            Log.v("Friends", "Friends Relation Saved");
                        } catch (ParseException pe){
                            pe.printStackTrace();
                            ToastManager.ShowToast(LoginActivity.this, pe.getMessage());
                        }
                        progressDialogManager.dismissDialog();
                        preferenceManager.saveUserName(PreferenceManager.USER_NAME, name);
                        preferenceManager.saveUserPicturePath(PreferenceManager.USER_PICTURE_PATH, picturePath);
                        startMainActivity();
                    } else {
                        e.printStackTrace();
                        if(e.getMessage().equalsIgnoreCase("invalid login parameters")){
                            new LogoutTask().execute();
                        } else{
                            signUpUserOnParse(prepareUserObject(object), object);
                        }
                    }
                }
            });
        } catch (JSONException e){
            e.printStackTrace();
            ToastManager.ShowToast(LoginActivity.this, e.getMessage());
        }
    }

    private class LogoutTask extends AsyncTask<Void, Void, Boolean>{

        @Override
        protected Boolean doInBackground(Void... params) {
            LoginManager loginManager = LoginManager.getInstance();
            if(loginManager != null){
                loginManager.logOut();
                return true;
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean logoutDone) {
            super.onPostExecute(logoutDone);
            progressDialogManager.dismissDialog();
            Toast.makeText(LoginActivity.this, "This user has already registered as an owner", Toast.LENGTH_LONG).show();
        }
    }

    private class SaveUserPictureTask extends AsyncTask<JSONObject, Void, JSONObject> {
        @Override
        protected JSONObject doInBackground(JSONObject... urls) {
            try {
                JSONObject object = urls[0];
                URL imageURL = new URL("https://graph.facebook.com/" + object.getString("id") + "/picture?type=large");
                Bitmap bitmap = BitmapFactory.decodeStream(imageURL.openConnection().getInputStream());
                File image = new File(Environment.getExternalStorageDirectory() + "/CuraGuard");
                if(!image.exists())
                    image.mkdir();
                image = new File(Environment.getExternalStorageDirectory() + "/CuraGuard", StringUtils.USER_PROFILE_PICTURE_NAME);
                FileOutputStream outStream = new FileOutputStream(image);
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, outStream);
                outStream.flush();
                outStream.close();
                return object;
            } catch (MalformedURLException e){
                e.printStackTrace();
                ToastManager.ShowToast(LoginActivity.this, e.getMessage());
            } catch (IOException e){
                e.printStackTrace();
                ToastManager.ShowToast(LoginActivity.this, e.getMessage());
            } catch (JSONException e){
                e.printStackTrace();
                ToastManager.ShowToast(LoginActivity.this, e.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(JSONObject object) {
            onFacebookLoginDone(object);
        }
    }

    private User prepareUserObject(JSONObject object){
        User user = new User();
        try {
            String firstName = "", lastName = "";
            String names[] = object.getString("name").split(" ");
            for (int i = 0; i < names.length; i++){
                if(i==0){
                    firstName = names[0];
                } else{
                    lastName = names[1];
                }
            }

            user.setName(object.getString("name"));
            user.setFacebookId(object.getString("id"));
            user.setGender(object.getString("gender"));
            //user.setEmail(object.getString("email"));
            user.setType("Volunteer");
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setPassword(CuraGuardApplication.PARSE_PASSWORD);
            user.setInstallationId(ParseInstallation.getCurrentInstallation().getInstallationId());
        } catch (JSONException e){
            e.printStackTrace();
            ToastManager.ShowToast(LoginActivity.this, e.getMessage());
        }
        return user;
    }

    private void signUpUserOnParse(final User fbUser, final JSONObject object){
        ParseUser user = new ParseUser();
        user.setUsername(fbUser.getName());
        user.setPassword(fbUser.getPassword());
        user.put(DatabaseUtils.Users_FIRST_NAME, fbUser.getFirstName());
        user.put(DatabaseUtils.Users_LAST_NAME, fbUser.getLastName());
        user.put(DatabaseUtils.Users_GENDER, fbUser.getGender());
        //user.put(DatabaseUtils.Users_EMAIL, fbUser.getEmail());
        user.put(DatabaseUtils.Users_FBID, fbUser.getFacebookId());
        user.put(DatabaseUtils.Users_TYPE, fbUser.getType());
        user.put(DatabaseUtils.Users_INSTALLATION_ID, fbUser.getInstallationId());
        //user.put(DatabaseUtils.Users_AUTH_DATA, accessToken);
        user.signUpInBackground(new SignUpCallback() {
                public void done(ParseException e) {
                    if (e == null) {
                        onFacebookLoginDone(object);
                    } else {
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(), "Parse Sign up Error", Toast.LENGTH_LONG).show();
                    }
                }
        });
    }

    private ArrayList<ParseUser> fetchUserFriendsFromParse(JSONObject object){
        final ArrayList<ParseUser> parseUsers = new ArrayList<ParseUser>();
        try {
            JSONArray friendsArray = object.getJSONObject("friends").getJSONArray("data");
            for (int i = 0; i < friendsArray.length(); i++) {
                JSONObject friendObject = friendsArray.getJSONObject(i);
                ParseQuery<ParseUser> query = ParseUser.getQuery();
                query.whereEqualTo(DatabaseUtils.Users_FBID, friendObject.getString("id"));
                List<ParseUser> fetchedFriends = query.find();
                if(fetchedFriends.size() > 0)
                    parseUsers.add(fetchedFriends.get(0));
            }
            Log.v("Friends", ""+parseUsers.toString());
        } catch (JSONException exception){
            exception.printStackTrace();
            ToastManager.ShowToast(LoginActivity.this, exception.getMessage());
        } catch (ParseException pe){
            pe.printStackTrace();
            ToastManager.ShowToast(LoginActivity.this, pe.getMessage());
        }
        return parseUsers;
    }

    private void startMainActivity(){
        preferenceManager.setUserLoggedIn(PreferenceManager.USER_LOGGED_IN, true);
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
