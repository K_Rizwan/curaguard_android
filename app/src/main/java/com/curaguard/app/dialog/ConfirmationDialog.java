package com.curaguard.app.dialog;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.curaguard.app.R;
import com.curaguard.app.interfaces.OnSendClickListener;

/**
 * Created by Kashif on 4/30/2015.
 */
public class ConfirmationDialog extends DialogFragment {

    private OnSendClickListener onSendClickListener;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.confirmation_dialog, container, false);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().setCanceledOnTouchOutside(false);

        Button btnSend = (Button)rootView.findViewById(R.id.button_send);
        Button btnSetReminder = (Button)rootView.findViewById(R.id.button_setReminder);
        final EditText etMessage = (EditText)rootView.findViewById(R.id.editText_Comment);
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(etMessage.getText().toString().equals("")){
                    Toast.makeText(getActivity(), "Please Enter Comment First", Toast.LENGTH_SHORT).show();
                } else {
                    onSendClickListener.onSendClicked(etMessage.getText().toString());
                    dismiss();
                }
            }
        });
        btnSetReminder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSendClickListener.onSetReminderClicked();
                dismiss();
            }
        });

        return rootView;
    }

    public void setOnSendClickListener(OnSendClickListener onSendClickListener){
        this.onSendClickListener = onSendClickListener;
    }
}
