package com.curaguard.app.utils;

import android.content.Context;

import com.curaguard.app.models.TasksModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

/**
 * Created by Kashif on 5/10/2015.
 */
public class RemindersJsonManager {

    private static final String REMINDER_ARRAY = "ReminderArray";
    private static final String OWNER_NAME = "OwnerName";
    private static final String OWNER_FB_ID = "OwnerFBId";
    private static final String JOB_TIME = "JobTime";
    private static final String JOB_DATE = "JobDate";
    private static final String JOB_ID = "JobId";

    PreferenceManager preferenceManager;
    public RemindersJsonManager(Context context){
        preferenceManager = new PreferenceManager(context);
    }

    public void addReminderToList(JSONObject object){
        try {
            String oldReminders = preferenceManager.fetchAllJobReminders();
            if(oldReminders.equals("")){
                JSONObject remindersObject = new JSONObject();
                JSONArray remindersJsonArray = new JSONArray();
                remindersJsonArray.put(object);
                remindersObject.put(REMINDER_ARRAY, remindersJsonArray);
                preferenceManager.saveJobReminders(remindersObject.toString());
            } else {
                JSONObject remindersObject = new JSONObject(oldReminders);
                JSONArray remindersJsonArray = remindersObject.getJSONArray(REMINDER_ARRAY);
                remindersJsonArray.put(object);
                remindersObject.put(REMINDER_ARRAY, remindersJsonArray);
                preferenceManager.saveJobReminders(remindersObject.toString());
            }
        } catch (JSONException e){
            e.printStackTrace();
        }
    }

    public JSONArray fetchAllReminders(){
        try {
            String allReminders = preferenceManager.fetchAllJobReminders();
            JSONObject remindersObject = new JSONObject(allReminders);
            JSONArray remindersJsonArray = remindersObject.getJSONArray(REMINDER_ARRAY);
            return remindersJsonArray;
        } catch (JSONException e){
            e.printStackTrace();
        }
        return null;
    }

    public JSONObject convertJobToJsonObject(TasksModel tasksModel){
        try {
            JSONObject object = new JSONObject();
            object.put(OWNER_NAME, tasksModel.getOwnerName());
            object.put(OWNER_FB_ID, tasksModel.getOwnerFBId());
            object.put(JOB_ID, tasksModel.getTaskId());
            object.put(JOB_TIME, tasksModel.getStartTime());
            object.put(JOB_DATE, tasksModel.getStartDate().getTime());
            return object;
        } catch (JSONException e){
            e.printStackTrace();
        }
        return null;
    }

    public TasksModel convertJsonToTaskModel(JSONObject object){
        try {
            TasksModel tasksModel = new TasksModel();
            tasksModel.setTaskId(object.getString(JOB_ID));
            tasksModel.setOwnerName(object.getString(OWNER_NAME));
            tasksModel.setStartDate(new Date(object.getLong(JOB_DATE)));
            tasksModel.setStartTime(object.getString(JOB_TIME));
            tasksModel.setOwnerFBId(object.getString(OWNER_FB_ID));
            return tasksModel;
        } catch (JSONException e){
            e.printStackTrace();
        }
        return null;
    }
}
