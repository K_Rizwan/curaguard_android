package com.curaguard.app.utils;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by Kashif on 5/19/2015.
 */
public class ToastManager {
    public static final void ShowToast(Context context, String message){
        Toast.makeText(context, ""+message, Toast.LENGTH_LONG).show();
    }
}
