package com.curaguard.app.utils;

/**
 * Created by Kashif on 5/14/2015.
 */
public class TaskAssigningStatus {
    public static final String PENDING = "Pending";
    public static final String REJECTED = "Rejected";
    public static final String Selected = "Selected";
}
