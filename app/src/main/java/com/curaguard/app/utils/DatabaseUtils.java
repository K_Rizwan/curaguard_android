package com.curaguard.app.utils;

/**
 * Created by Kashif on 4/28/2015.
 */
public class DatabaseUtils {
    public static final String TABLE_TASKS = "Tasks";
    public static final String TABLE_CATEGORY = "Category";
    public static final String TABLE_OWNERS = "Owners";
    public static final String TABLE_USER = "User";
    public static final String TABLE_RATING = "Rating";
    public static final String TABLE_COMMENT = "Comment";
    public static final String TABLE_FOLLOWED_FRIENDS = "FollowedFriends";
    public static final String TABLE_TASKS_ACCEPTED = "TasksAcccepted";
    public static final String TABLE_TASKS_VOLUNTEERS = "TasksVolunteers";

    public static final String TASKS_STATUS = "status";
    public static final String TASKS_TITLE = "title";
    public static final String TASKS_START_TIME = "startTime";
    public static final String TASKS_START_DATE = "startDate";
    //public static final String TASKS_ADDRESS = "address";
    //public static final String TASKS_DESCRIPTION = "description";
    public static final String TASKS_CREATED_BY = "CreatedBy";

    public static final String OWNERS_NAME = "ownerName";

    public static final String Users_AUTH_DATA = "authData";
    public static final String Users_OBJECT_ID = "objectId";
    public static final String Users_FBID = "fbID";
    public static final String Users_NAME = "username";
    public static final String Users_FIRST_NAME = "firstname";
    public static final String Users_LAST_NAME = "lastname";
    public static final String Users_FRIENDS = "friends";
    public static final String Users_GENDER = "gender";
    public static final String Users_EMAIL = "email";
    public static final String Users_TYPE = "type";
    public static final String Users_FB_ID = "fbID";
    public static final String Users_IS_FOLLOWING = "isFollowing";
    public static final String Users_INSTALLATION_ID = "installationId";

    public static final String TASK_ACCEPTED_STATUS = "status";
    public static final String TASK_ACCEPTED_DESCRIPTION = "description";
    public static final String TASK_ACCEPTED_USER_ID = "userID";
    public static final String TASK_ACCEPTED_TASK_ID = "taskID";

    public static final String RATING_TASK_ID = "taskId";
    public static final String RATING_RATING = "rating";

    public static final String COMMENT_MESSAGE = "message";
    public static final String COMMENT_TASK = "commentTask";
    public static final String COMMENT_SENDER = "sender";
    public static final String COMMENT_RECEIVER = "receiver";
    public static final String COMMENT_CREATED_AT = "createdAt";

    public static final String TASKS_VOLUNTEER_USER = "userObjectID";
    public static final String TASKS_VOLUNTEER_OWNER = "ownerObjectID";
    public static final String TASKS_VOLUNTEER_TASK = "taskObjectID";
    public static final String TASKS_VOLUNTEER_STATUS = "status";
    public static final String TASKS_VOLUNTEER_TASK_STATUS = "taskStatus";

    public static final String FOLLOWED_FRIENDS_CURRENT_USER = "currentUser";
    public static final String FOLLOWED_FRIENDS_FRIEND_USER = "friendUser";
    public static final String FOLLOWED_FRIENDS_IS_FOLLOWED = "isFollowed";
}
