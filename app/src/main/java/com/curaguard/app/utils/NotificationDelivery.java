package com.curaguard.app.utils;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;

/**
 * Created by vladimirhudnitsky.
 */
public class NotificationDelivery {
    private boolean vibrate = true;
    private boolean withSound = true;

    public NotificationDelivery(boolean vibrate, boolean withSound) {
        this.vibrate = vibrate;
        this.withSound = withSound;
    }

    public void sendNotification(Context context, int id, String message){
        final Notification notification = new NotificationBuilder(context).buildNotification(context, message, new Bundle());
        deliverNotification(context, id, notification);
    }

    private void deliverNotification(Context context, int id, Notification notification) {

        //return if in setting disable notification feature
        if (notification == null) {
            return;
        }

        final NotificationManager notificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(id, notification);

        final boolean vibrate = true;
        long[] vibratePattern = new long[]{0, 300, 500, 300, 300, 0};

        if (vibrate) {
            //vibrate
            final Vibrator v = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
            v.vibrate(vibratePattern, -1);
        }

        final boolean withSound = true;
        if (withSound) {
            //play sound
            playRingtone(context);
        }
    }

    private void playRingtone(Context context) {
        try {
            final Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            final Ringtone r = RingtoneManager.getRingtone(context, notification);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
