package com.curaguard.app.utils;

import android.app.ProgressDialog;
import android.content.Context;

/**
 * Created by Kashif on 4/30/2015.
 */
public class ProgressDialogManager {

    private ProgressDialog progressDialog;
    public ProgressDialogManager(Context context){
        progressDialog = new ProgressDialog(context);
    }

    public void showDialog(String message){
        progressDialog.setMessage(message);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }

    public void dismissDialog(){
        if(progressDialog != null)
            progressDialog.dismiss();
    }
}
