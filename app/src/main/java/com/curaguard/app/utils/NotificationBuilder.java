package com.curaguard.app.utils;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;

import com.curaguard.app.MainActivity;
import com.curaguard.app.R;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class NotificationBuilder extends NotificationCompat.Builder {

    public NotificationBuilder(Context context) {
        super(context);
    }

    public Notification buildNotification(Context context, String alert, Bundle bundle) {
        return buildNotification(context, alert, createExtrasMap(bundle));
    }

    public Notification buildNotification(Context context, String alert, Map<String, String> extras) {
        final Bundle bundleExtras = new Bundle();
        final Iterator<Map.Entry<String, String>> iterator = extras.entrySet().iterator();
        while (iterator.hasNext()) {
            final Map.Entry<String, String> entry = iterator.next();
            bundleExtras.putString(entry.getKey(), entry.getValue());
        }
        this.setExtras(bundleExtras);

        this.setSmallIcon(R.mipmap.ic_launcher).setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher));
        this.setContentText(alert).setContentTitle(context.getResources().getString(R.string.app_name));

        final long when = System.currentTimeMillis();
        this.setWhen(when);
        this.setPriority(Notification.PRIORITY_HIGH);


        final Intent resultIntent = new Intent(context, MainActivity.class);
        final PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        context,
                        0,
                        resultIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        this.setContentIntent(resultPendingIntent);

        final Notification notification = this.build();
        notification.contentView.setImageViewResource(android.R.id.icon, R.mipmap.ic_launcher);
        return notification;

    }

    private Map<String, String> createExtrasMap(Bundle bundle) {
        final HashMap<String, String> extraMap = new HashMap<>();

        for (String key : bundle.keySet()) {
            extraMap.put(key, bundle.getString(key));
        }

        return extraMap;
    }
}