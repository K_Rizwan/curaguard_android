package com.curaguard.app.utils;

/**
 * Created by Kashif on 5/14/2015.
 */
public class TaskStatus {
    public static final String ACTIVE = "Active";
    public static final String OPEN = "Open";
    public static final String CLOSE = "Close";
}
