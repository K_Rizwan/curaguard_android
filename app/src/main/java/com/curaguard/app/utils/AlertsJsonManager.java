package com.curaguard.app.utils;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Kashif on 5/10/2015.
 */
public class AlertsJsonManager {

    private static final String JOB_ID = "JobId";
    private static final String TASK_ID_ARRAY = "TaskIdsArray";

    PreferenceManager preferenceManager;
    public AlertsJsonManager(Context context){
        preferenceManager = new PreferenceManager(context);
    }

    public void addTaskIdToList(String id){
        try {
            String oldTaskIds = preferenceManager.fetchAllJobsUpdateAlert();
            if(oldTaskIds.equals("")){
                JSONObject taskIdObject = new JSONObject();
                JSONArray taskIdsJsonArray = new JSONArray();
                taskIdsJsonArray.put(id);
                taskIdObject.put(TASK_ID_ARRAY, taskIdsJsonArray);
                preferenceManager.addJobUpdateAlert(taskIdObject.toString());
            } else {
                JSONObject taskIdObject = new JSONObject(oldTaskIds);
                JSONArray taskIdJsonArray = taskIdObject.getJSONArray(TASK_ID_ARRAY);
                taskIdJsonArray.put(id);
                taskIdObject.put(TASK_ID_ARRAY, taskIdJsonArray);
                preferenceManager.addJobUpdateAlert(taskIdObject.toString());
            }
        } catch (JSONException e){
            e.printStackTrace();
        }
    }

    public JSONArray fetchAllTaskIds(){
        try {
            String allTaskIds = preferenceManager.fetchAllJobsUpdateAlert();
            JSONObject taskIdObject = new JSONObject(allTaskIds);
            JSONArray taskIdsJsonArray = taskIdObject.getJSONArray(TASK_ID_ARRAY);
            return taskIdsJsonArray;
        } catch (JSONException e){
            e.printStackTrace();
        }
        return null;
    }

    public void deleteTaskIds(String id){
        try {
            String allTaskIds = preferenceManager.fetchAllJobsUpdateAlert();
            JSONObject taskIdObject = new JSONObject(allTaskIds);
            JSONArray taskIdsJsonArray = taskIdObject.getJSONArray(TASK_ID_ARRAY);
            if(taskIdsJsonArray != null && taskIdsJsonArray.length() > 0){
                JSONArray jsonArray = new JSONArray();
                for(int i = 0; i < taskIdsJsonArray.length(); i++){
                    if(!taskIdsJsonArray.getString(i).equals(id))
                        jsonArray.put(taskIdsJsonArray.get(i));
                }
                taskIdObject.put(TASK_ID_ARRAY, jsonArray);
                preferenceManager.addJobUpdateAlert(taskIdObject.toString());
            }

        } catch (JSONException e){
            e.printStackTrace();
        }
    }
}
