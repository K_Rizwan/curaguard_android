package com.curaguard.app.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Kashif on 4/27/2015.
 */
public class PreferenceManager {

    public static String USER_LOGGED_IN = "UserLoggedIn";
    public static final String USER_NAME = "UserName";
    public static final String USER_PICTURE_PATH = "PicturePath";
    public static final String USER_PICTURE = "Picture";
    public static final String REMINDERS_LIST = "ReminderList";
    public static final String UPDATE_ALERT = "UpdateAlert";
    public static final String COMMENT_FETCHING = "CommentFetching";

    private SharedPreferences sharedPreference;
    public PreferenceManager(Context context){
        sharedPreference = context.getSharedPreferences("CuraGuardPreferences", Context.MODE_PRIVATE);
    }

    public void setUserLoggedIn(String key, boolean loggedIn){
        sharedPreference.edit().putBoolean(key, loggedIn).apply();
    }

    public boolean isUserAlreadyLoggedIn(String key){
        return sharedPreference.getBoolean(key, false);
    }

    public void setAccepted(String key, boolean name){
        sharedPreference.edit().putBoolean(key, name).apply();
    }

    public boolean isAccepted(String key){
        return sharedPreference.getBoolean(key, false);
    }

    public void saveUserName(String key, String name){
        sharedPreference.edit().putString(key, name).apply();
    }

    public String getUserName(String key){
        return sharedPreference.getString(key, "");
    }

    public void setCommentsFetchingFirstTime(Boolean value){
        sharedPreference.edit().putBoolean(COMMENT_FETCHING, value).apply();
    }

    public Boolean isCommentsFetchingFirstTime(){
        return sharedPreference.getBoolean(COMMENT_FETCHING, false);
    }

    public void saveUserPicturePath(String key, String name){
        sharedPreference.edit().putString(key, name).apply();
    }

    public String getUserPicturePath(String key){
        return sharedPreference.getString(key, "");
    }

    public void saveJobReminders(String name){
        sharedPreference.edit().putString(REMINDERS_LIST, name).apply();
    }

    public String fetchAllJobReminders(){
        return sharedPreference.getString(REMINDERS_LIST, "");
    }

    public void addJobUpdateAlert(String name){
        sharedPreference.edit().putString(UPDATE_ALERT, name).apply();
    }

    public String fetchAllJobsUpdateAlert(){
        return sharedPreference.getString(UPDATE_ALERT, "");
    }
}
