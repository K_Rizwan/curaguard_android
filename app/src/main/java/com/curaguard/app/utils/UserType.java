package com.curaguard.app.utils;

/**
 * Created by Kashif on 5/26/2015.
 */
public class UserType {
    public static final String OWNER = "Owner";
    public static final String VOLUNTEER = "Volunteer";
}
