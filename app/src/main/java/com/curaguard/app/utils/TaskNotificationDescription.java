package com.curaguard.app.utils;

/**
 * Created by Kashif on 5/16/2015.
 */
public class TaskNotificationDescription {
    public static final String NEW_COMMENT_ON_TASK = "New comment on task";
    public static final String NEW_ACTIVITY_ON_TASK = "New activity on task";
}
