package com.curaguard.app;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.curaguard.app.R;
import com.curaguard.app.fragments.HomeFragment;
import com.curaguard.app.fragments.PeopleFragment;
import com.curaguard.app.fragments.ReminderFragment;
import com.curaguard.app.utils.PreferenceManager;
import com.curaguard.app.utils.ProgressDialogManager;
import com.facebook.login.LoginManager;
import com.facebook.share.model.AppInviteContent;
import com.facebook.share.widget.AppInviteDialog;


public class MainActivity extends Activity {

    private TextView tvHeaderTitle, tvHome, tvPeople, tvReminder, tvHeaderLogOut, tvHeaderRefresh;
    private ImageView ivHome, ivPeople, ivReminder, ivHeaderPlusButton;
    private PreferenceManager preferenceManager;
    private HomeFragment homeFragment;
    private PeopleFragment peopleFragment;
    private ReminderFragment reminderFragment;
    private ProgressDialogManager progressDialogManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        preferenceManager = new PreferenceManager(this);
        progressDialogManager = new ProgressDialogManager(this);
        initControls();

        tvHeaderTitle.setText(getResources().getString(R.string.profile));
        setBottomButtonActive(1);
        homeFragment = new HomeFragment();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.add(R.id.frameLayout, homeFragment);
        transaction.commit();
    }

    private void initControls(){
        tvHeaderTitle = (TextView)findViewById(R.id.textView_headerTitle);
        tvHome = (TextView)findViewById(R.id.textView_Home);
        tvPeople = (TextView)findViewById(R.id.textView_People);
        tvReminder = (TextView)findViewById(R.id.textView_Reminder);
        tvHeaderLogOut = (TextView)findViewById(R.id.textView_logoutButton);
        tvHeaderRefresh = (TextView)findViewById(R.id.textView_refreshButton);
        ivHeaderPlusButton = (ImageView)findViewById(R.id.imageView_PlusButton);
        ivHome = (ImageView)findViewById(R.id.imageView_Home);
        ivPeople = (ImageView)findViewById(R.id.imageView_People);
        ivReminder = (ImageView)findViewById(R.id.imageView_Reminder);

        tvHeaderLogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new LogoutTask().execute();
            }
        });
        ivHeaderPlusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String appLinkUrl = "https://www.mydomain.com/myapplink";
                String previewImageUrl = "https://www.mydomain.com/my_invite_image.jpg";

                if (AppInviteDialog.canShow()) {
                    AppInviteContent content = new AppInviteContent.Builder()
                            .setApplinkUrl(appLinkUrl)
                            .setPreviewImageUrl(previewImageUrl)
                            .build();
                    AppInviteDialog.show(MainActivity.this, content);
                }
            }
        });
        tvHeaderRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(homeFragment != null)
                    homeFragment.refreshAllTasks();
            }
        });
    }

    private class LogoutTask extends AsyncTask<Void, Void, Boolean>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialogManager.showDialog("Log out in process");
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            LoginManager loginManager = LoginManager.getInstance();
            if(loginManager != null){
                loginManager.logOut();
                return true;
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean logoutDone) {
            super.onPostExecute(logoutDone);
            progressDialogManager.dismissDialog();
            if(logoutDone){
                preferenceManager.setUserLoggedIn(PreferenceManager.USER_LOGGED_IN, false);
                startActivity(new Intent(MainActivity.this, LoginActivity.class));
                finish();
            }
        }
    }

    public void onHomeButtonClicked(View v){
        tvHeaderTitle.setText(getResources().getString(R.string.profile));
        setBottomButtonActive(1);
        homeFragment = new HomeFragment();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.add(R.id.frameLayout, homeFragment);
        transaction.commit();
    }

    public void onPeopleButtonClicked(View v){
        tvHeaderTitle.setText(getResources().getString(R.string.people));
        setBottomButtonActive(2);
        peopleFragment = new PeopleFragment();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.add(R.id.frameLayout, peopleFragment);
        transaction.commit();
    }

    public void onReminderButtonClicked(View v){
        tvHeaderTitle.setText(getResources().getString(R.string.reminders));
        setBottomButtonActive(3);
        reminderFragment = new ReminderFragment();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.add(R.id.frameLayout, reminderFragment);
        transaction.commit();
    }

    private void setBottomButtonActive(int i){
        ivHome.setImageResource(R.drawable.home_logo_idle);
        tvHome.setTextColor(getResources().getColor(R.color.Grey_Text_BottomBar));
        ivPeople.setImageResource(R.drawable.people_logo_idle);
        tvPeople.setTextColor(getResources().getColor(R.color.Grey_Text_BottomBar));
        ivReminder.setImageResource(R.drawable.reminder_logo_idle);
        tvReminder.setTextColor(getResources().getColor(R.color.Grey_Text_BottomBar));
        switch (i){
            case 1:
                ivHeaderPlusButton.setVisibility(View.GONE);
                tvHeaderLogOut.setVisibility(View.VISIBLE);
                tvHeaderRefresh.setVisibility(View.VISIBLE);
                ivHome.setImageResource(R.drawable.home_logo_active);
                tvHome.setTextColor(getResources().getColor(R.color.Green_Text));
                break;
            case 2:
                ivHeaderPlusButton.setVisibility(View.VISIBLE);
                tvHeaderLogOut.setVisibility(View.GONE);
                tvHeaderRefresh.setVisibility(View.GONE);
                ivPeople.setImageResource(R.drawable.people_logo_active);
                tvPeople.setTextColor(getResources().getColor(R.color.Green_Text));
                break;
            case 3:
                ivHeaderPlusButton.setVisibility(View.GONE);
                tvHeaderLogOut.setVisibility(View.GONE);
                tvHeaderRefresh.setVisibility(View.GONE);
                ivReminder.setImageResource(R.drawable.reminder_logo_active);
                tvReminder.setTextColor(getResources().getColor(R.color.Green_Text));
                break;
            default:
                break;
        }
    }
}
