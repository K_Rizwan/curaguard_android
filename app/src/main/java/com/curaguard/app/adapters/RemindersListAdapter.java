package com.curaguard.app.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.curaguard.app.R;
import com.curaguard.app.models.TasksModel;
import com.curaguard.app.utils.ImageUtils;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by Kashif on 4/12/2015.
 */
public class RemindersListAdapter extends BaseAdapter {

    Context context;
    List<TasksModel> reminderData;

    public RemindersListAdapter(Context context, List<TasksModel> items) {
        this.context = context;
        this.reminderData = items;
    }

    @Override
    public int getCount() {
        return reminderData.size();
    }

    @Override
    public Object getItem(int position) {
        return reminderData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return reminderData.indexOf(getItem(position));
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        TasksModel tasksModel = reminderData.get(position);

        LayoutInflater mInflater = (LayoutInflater)context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.reminders_list_item, null);
            holder = new ViewHolder();
            holder.textViewOwnerName = (TextView) convertView.findViewById(R.id.textView_userNameOnList);
            holder.textViewJobDate = (TextView) convertView.findViewById(R.id.textView_Date);
            holder.textViewJobTime = (TextView) convertView.findViewById(R.id.textView_PickUpTime);
            holder.ivOwnerPhoto = (ImageView) convertView.findViewById(R.id.imageView_userIcon);
            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.textViewOwnerName.setText(tasksModel.getOwnerName());
        holder.textViewJobTime.setText("Pickup at: " + tasksModel.getStartTime());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMM, yyyy");
        holder.textViewJobDate.setText(simpleDateFormat.format(tasksModel.getStartDate()));
        new FetchOwnerPictureTask(holder.ivOwnerPhoto).execute(tasksModel.getOwnerFBId());

        return convertView;
    }

    /*private view holder class*/
    private class ViewHolder {
        TextView textViewOwnerName;
        TextView textViewJobTime;
        TextView textViewJobDate;
        ImageView ivOwnerPhoto;
    }

    private class FetchOwnerPictureTask extends AsyncTask<String, Void, Bitmap> {

        private ImageView ivOwnerPicture;
        public FetchOwnerPictureTask(ImageView ivOwnerPicture){
            this.ivOwnerPicture = ivOwnerPicture;
        }

        @Override
        protected Bitmap doInBackground(String... urls) {
            Bitmap bitmap = null;
            try {
                String facebookId = urls[0];
                URL imageURL = new URL("https://graph.facebook.com/" + facebookId + "/picture?type=small");
                bitmap = BitmapFactory.decodeStream(imageURL.openConnection().getInputStream());
            } catch (MalformedURLException e){
                e.printStackTrace();
            } catch (IOException e){
                e.printStackTrace();
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            if (result != null)
                ivOwnerPicture.setImageBitmap(ImageUtils.cropToCircularImage(result));
        }
    }
}
