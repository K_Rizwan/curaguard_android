package com.curaguard.app.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.curaguard.app.R;
import com.curaguard.app.utils.DatabaseUtils;
import com.curaguard.app.utils.ImageUtils;
import com.curaguard.app.utils.ProgressDialogManager;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

/**
 * Created by Kashif on 4/12/2015.
 */
public class PeopleListAdapter extends BaseAdapter {

    private Context context;
    private List<ParseUser> peopleData;
    private ProgressDialogManager progressDialogManager;

    public PeopleListAdapter(Context context, List<ParseUser> items) {
        this.context = context;
        this.peopleData = items;
        progressDialogManager = new ProgressDialogManager(context);
    }

    @Override
    public int getCount() {
        return peopleData.size();
    }

    @Override
    public Object getItem(int position) {
        return peopleData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return peopleData.indexOf(getItem(position));
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        final ParseUser parseUser = peopleData.get(0);

        LayoutInflater mInflater = (LayoutInflater)context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.people_list_item, null);
            holder = new ViewHolder();
            holder.textViewPeopleName = (TextView) convertView.findViewById(R.id.textView_PeopleName);
            holder.ivFriendPhoto = (ImageView) convertView.findViewById(R.id.imageView_people);
            holder.switchFollow = (Switch) convertView.findViewById(R.id.switch_Follow);
            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.textViewPeopleName.setText(parseUser.getString(DatabaseUtils.Users_NAME));
        new FetchOwnerPictureTask(holder.ivFriendPhoto).execute(parseUser.getString(DatabaseUtils.Users_FBID));
        holder.switchFollow.setChecked(parseUser.getBoolean(DatabaseUtils.Users_IS_FOLLOWING));
        if(holder.switchFollow.isChecked())
            holder.switchFollow.setText("Follow");
        else
            holder.switchFollow.setText("UnFollow");
        final ViewHolder viewHolder = holder;
        holder.switchFollow.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    new ChangeFollowingOnParse(false).execute(parseUser);
                    viewHolder.switchFollow.setText("Follow");
                } else{
                    new ChangeFollowingOnParse(true).execute(parseUser);
                    viewHolder.switchFollow.setText("UnFollow");
                }
            }
        });

        return convertView;
    }

    private class ChangeFollowingOnParse extends AsyncTask<ParseUser, Void, Boolean> {
        private boolean isFollowing;
        public ChangeFollowingOnParse(boolean isFollowing){
            this.isFollowing = isFollowing;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialogManager.showDialog("Please Wait");
        }

        @Override
        protected Boolean doInBackground(ParseUser... urls) {
            try {
                ParseUser friendUser = urls[0];
                ParseUser currentUser = ParseUser.getCurrentUser();
                ParseQuery<ParseObject> query = ParseQuery.getQuery(DatabaseUtils.TABLE_FOLLOWED_FRIENDS);
                query.whereEqualTo(DatabaseUtils.FOLLOWED_FRIENDS_CURRENT_USER, currentUser);
                query.whereEqualTo(DatabaseUtils.FOLLOWED_FRIENDS_FRIEND_USER, friendUser);
                ParseObject parseObject = query.getFirst();
                parseObject.put(DatabaseUtils.FOLLOWED_FRIENDS_IS_FOLLOWED, isFollowing);
                parseObject.save();
                /*ParseRelation<ParseUser> relation = user.getRelation("friends");
                List<ParseUser> userFriends = relation.getQuery().find();
                for(ParseUser parseUser : userFriends){
                    if(urls[0].equalsIgnoreCase(parseUser.getObjectId()));{
                        parseUser.put(DatabaseUtils.Users_IS_FOLLOWING, isFollowing);
                        //parseUser.save();
                        relation.remove(parseUser);
                        relation.add(parseUser);
                        user.save();
                        break;
                    }
                }*/
                return true;
            } catch (ParseException e){
                e.printStackTrace();
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            progressDialogManager.dismissDialog();
        }
    }

    /*private view holder class*/
    private class ViewHolder {
        TextView textViewPeopleName;
        ImageView ivFriendPhoto;
        Switch switchFollow;
    }

    private class FetchOwnerPictureTask extends AsyncTask<String, Void, Bitmap> {

        private ImageView ivOwnerPicture;
        public FetchOwnerPictureTask(ImageView ivOwnerPicture){
            this.ivOwnerPicture = ivOwnerPicture;
        }

        @Override
        protected Bitmap doInBackground(String... urls) {
            Bitmap bitmap = null;
            try {
                String facebookId = urls[0];
                URL imageURL = new URL("https://graph.facebook.com/" + facebookId + "/picture?type=large");
                bitmap = BitmapFactory.decodeStream(imageURL.openConnection().getInputStream());
            } catch (MalformedURLException e){
                e.printStackTrace();
            } catch (IOException e){
                e.printStackTrace();
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            if (result != null)
                ivOwnerPicture.setImageBitmap(ImageUtils.cropToCircularImage(result));
        }
    }
}
