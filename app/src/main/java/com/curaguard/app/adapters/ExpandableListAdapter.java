package com.curaguard.app.adapters;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.curaguard.app.R;
import com.curaguard.app.dialog.ConfirmationDialog;
import com.curaguard.app.interfaces.AcceptanceButtonClickListener;
import com.curaguard.app.interfaces.OnSendClickListener;
import com.curaguard.app.models.TasksModel;
import com.curaguard.app.services.NotificationHandlerService;
import com.curaguard.app.services.PostCommentIntentService;
import com.curaguard.app.utils.AlertsJsonManager;
import com.curaguard.app.utils.DatabaseUtils;
import com.curaguard.app.utils.ImageUtils;
import com.curaguard.app.utils.KeyboardUtils;
import com.curaguard.app.utils.PreferenceManager;
import com.curaguard.app.utils.ProgressDialogManager;
import com.curaguard.app.utils.RemindersJsonManager;
import com.curaguard.app.utils.StringUtils;
import com.curaguard.app.utils.TaskAssigningStatus;
import com.curaguard.app.utils.TaskNotificationDescription;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Kashif on 4/8/2015.
 */
public class ExpandableListAdapter extends BaseExpandableListAdapter{

    private Activity context;
    private List<String> listDataHeader;
    private PreferenceManager preferenceManager;
    private AlertsJsonManager alertsJsonManager;
    private ProgressDialogManager progressDialogManager;
    private List<String> temporaryRejectedTasks;
    private HashMap<String, List<TasksModel>> listDataChild;
    private AcceptanceButtonClickListener acceptanceButtonClickListener;

    public ExpandableListAdapter(Activity context, List<String> listDataHeader, HashMap<String,
            List<TasksModel>> listChildData) {
        this.context = context;
        this.listDataHeader = listDataHeader;
        this.listDataChild = listChildData;
        progressDialogManager = new ProgressDialogManager(context);
        preferenceManager = new PreferenceManager(context);
        alertsJsonManager = new AlertsJsonManager(context);
        temporaryRejectedTasks = new ArrayList<>();
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return this.listDataChild.get(this.listDataHeader.get(groupPosition)).get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView,
                             ViewGroup parent) {

        List<String> alertTaskIds = new ArrayList<>();
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.jobs_list_item, null);
        }

        try {
            alertTaskIds = new ArrayList<String>();
            JSONArray alertsJsonArray = alertsJsonManager.fetchAllTaskIds();
            if(alertsJsonArray != null && alertsJsonArray.length() > 0){
                for(int i = 0; i < alertsJsonArray.length(); i++){
                    alertTaskIds.add(alertsJsonArray.getString(i));
                }
            }
        } catch (JSONException e){
            e.printStackTrace();
        }

        TextView textViewOwnerName = (TextView) convertView.findViewById(R.id.textView_userNameOnList);
        TextView textViewDate =  (TextView) convertView.findViewById(R.id.textView_Date);
        TextView textViewTask = (TextView) convertView.findViewById(R.id.textView_Task);
        TextView textViewAlertIcon = (TextView) convertView.findViewById(R.id.textView_AlertIcon);
        RatingBar ratingBar = (RatingBar)convertView.findViewById(R.id.ratingBar);
        ImageView ivCenterBlock = (ImageView)convertView.findViewById(R.id.imageView_centeredColoredBlock);
        ImageView ivUserIcon = (ImageView)convertView.findViewById(R.id.imageView_userIcon);
        final ImageView ivAcceptJob = (ImageView)convertView.findViewById(R.id.imageView_acceptJob);
        final ImageView ivRejectJob = (ImageView)convertView.findViewById(R.id.imageView_rejectJob);
        //TextView textViewAddress = (TextView) convertView.findViewById(R.id.textView_Address);
        TextView textViewDetail = (TextView) convertView.findViewById(R.id.textView_TaskDetail);
        TextView textViewTime = (TextView) convertView.findViewById(R.id.textView_TimeFrame);
        LinearLayout llAcceptanceButtons = (LinearLayout)convertView.findViewById(R.id.linearLayout_acceptanceButtons);
        final RelativeLayout relativeLayoutMainListItem = (RelativeLayout)convertView.findViewById(R.id.relativeLayout_mainListItem);
        RelativeLayout rlTaskTimeAndPickupAddress = (RelativeLayout) convertView.findViewById(R.id.relativeLayout_taskTimeAndPickupAddress);

        List<TasksModel> tasksModels = listDataChild.get(listDataHeader.get(groupPosition));
        final TasksModel tasksModel = tasksModels.get(childPosition);

        if(alertTaskIds.contains(tasksModel.getTaskId()))
            textViewAlertIcon.setVisibility(View.VISIBLE);
        else
            textViewAlertIcon.setVisibility(View.GONE);

        switch (groupPosition){
            case 0:
                relativeLayoutMainListItem.setBackgroundColor(context.getResources().getColor(R.color.Red_Shade));
                ivCenterBlock.setBackgroundResource(R.drawable.red_block);
                rlTaskTimeAndPickupAddress.setVisibility(View.GONE);
                textViewTask.setVisibility(View.VISIBLE);
                llAcceptanceButtons.setVisibility(View.GONE);
                ratingBar.setVisibility(View.GONE);

                textViewTask.setText(tasksModel.getTitle());
                break;
            case 1:
                relativeLayoutMainListItem.setBackgroundColor(context.getResources().getColor(R.color.Green_Shade));
                ivCenterBlock.setBackgroundResource(R.drawable.green_block);
                rlTaskTimeAndPickupAddress.setVisibility(View.VISIBLE);
                textViewTask.setVisibility(View.GONE);
                llAcceptanceButtons.setVisibility(View.VISIBLE);
                ratingBar.setVisibility(View.GONE);

                textViewTime.setText(tasksModel.getStartTime());
                textViewDetail.setText(tasksModel.getTitle());
                if(tasksModel.getTaskAssigningStatus().equals(TaskAssigningStatus.PENDING)){
                    ivAcceptJob.setImageResource(R.drawable.yes_active);
                    ivRejectJob.setImageResource(R.drawable.no_idle);
                } else{
                    ivAcceptJob.setImageResource(R.drawable.yes_idle);
                    ivRejectJob.setImageResource(R.drawable.no_active);
                }
                ivAcceptJob.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ConfirmationDialog confirmationDialog = new ConfirmationDialog();
                        confirmationDialog.setOnSendClickListener(new OnSendClickListener() {
                            @Override
                            public void onSendClicked(String message) {
                                ivAcceptJob.setImageResource(R.drawable.yes_active);
                                ivRejectJob.setImageResource(R.drawable.no_idle);

                                KeyboardUtils.hideKeyboard(context);
                                tasksModel.setTaskAssigningStatus(TaskAssigningStatus.PENDING);
                                preferenceManager.setAccepted(tasksModel.getTaskId(), true);
                                acceptanceButtonClickListener.onAcceptButtonClicked(groupPosition, childPosition);
                                saveCommentOnParse(true, message, tasksModel);
                            }

                            @Override
                            public void onSetReminderClicked() {
                                if(tasksModel.getStartDate().after(new Date())){
                                    RemindersJsonManager remindersJsonManager = new RemindersJsonManager(context);
                                    Calendar calendar = Calendar.getInstance();
                                    //calendar.setTimeInMillis(tasksModel.getStartDate().getTime() - (24 * 60 * 60 * 1000));
                                    calendar.setTimeInMillis(tasksModel.getStartDate().getTime() - (60 * 60 * 1000));
                                    scheduleReminder(calendar, (int)tasksModel.getStartDate().getTime(), tasksModel.getTitle());
                                    JSONObject object = remindersJsonManager.convertJobToJsonObject(tasksModel);
                                    remindersJsonManager.addReminderToList(object);
                                }
                            }
                        });
                        confirmationDialog.show(context.getFragmentManager(), ConfirmationDialog.class.getName());
                    }
                });
                ivRejectJob.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new AlertDialog.Builder(context)
                                .setTitle("Alert")
                                .setMessage("Are you sure you want to reject this job?")
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        ivAcceptJob.setImageResource(R.drawable.yes_idle);
                                        ivRejectJob.setImageResource(R.drawable.no_active);

                                        ivAcceptJob.setEnabled(false);
                                        ivRejectJob.setEnabled(false);
                                        relativeLayoutMainListItem.setAlpha(0.7f);

                                        tasksModel.setTaskAssigningStatus(TaskAssigningStatus.REJECTED);
                                        temporaryRejectedTasks.add(tasksModel.getTaskId());
                                        preferenceManager.setAccepted(tasksModel.getTaskId(), false);
                                        new SaveAcceptedTaskOnParse(false, tasksModel.getTaskVolunteerId()).execute();
                                    }
                                })
                                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {}
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
                    }
                });
                if(temporaryRejectedTasks.contains(tasksModel.getTaskId())){
                    ivAcceptJob.setEnabled(false);
                    ivRejectJob.setEnabled(false);
                    relativeLayoutMainListItem.setAlpha(0.7f);
                } else{
                    ivAcceptJob.setEnabled(true);
                    ivRejectJob.setEnabled(true);
                    relativeLayoutMainListItem.setAlpha(1.0f);
                }
                break;
            case 2:
                relativeLayoutMainListItem.setBackgroundColor(context.getResources().getColor(R.color.Blue_Shade));
                ivCenterBlock.setBackgroundResource(R.drawable.blue_block);
                rlTaskTimeAndPickupAddress.setVisibility(View.GONE);
                textViewTask.setVisibility(View.GONE);
                llAcceptanceButtons.setVisibility(View.INVISIBLE);
                ratingBar.setVisibility(View.VISIBLE);

                ratingBar.setRating(tasksModel.getRating());
                break;
        }

        textViewOwnerName.setText(tasksModel.getOwnerName());
        new FetchOwnerPictureTask(ivUserIcon).execute(tasksModel.getOwnerFBId());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMM, yyyy");
        textViewDate.setText(simpleDateFormat.format(tasksModel.getStartDate()));
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this.listDataChild.get(this.listDataHeader.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this.listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.jobs_list_group_header, null);
        }

        TextView lblListHeader = (TextView) convertView.findViewById(R.id.textView_headerText);
        RelativeLayout listGroupHeader = (RelativeLayout) convertView.findViewById(R.id.relativeLayout_listGroupHeader);

        lblListHeader.setText(headerTitle);
        switch (groupPosition){
            case 0:
                listGroupHeader.setBackgroundColor(context.getResources().getColor(R.color.Red));
                break;
            case 1:
                listGroupHeader.setBackgroundColor(context.getResources().getColor(R.color.Green));
                break;
            case 2:
                listGroupHeader.setBackgroundColor(context.getResources().getColor(R.color.Blue));
                break;
            default:
                break;
        }

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    private void saveCommentOnParse(boolean isAccepted, final String message, final TasksModel tasksModel){
        progressDialogManager.showDialog("Please Wait");
        ParseUser taskOwner = StringUtils.ACTIVE_PARSE_TASK.getParseUser(DatabaseUtils.TASKS_CREATED_BY);
        ParseUser currentUser = ParseUser.getCurrentUser();
        ParseObject commentObject = new ParseObject(DatabaseUtils.TABLE_COMMENT);
        commentObject.put(DatabaseUtils.COMMENT_MESSAGE, message);
        commentObject.put(DatabaseUtils.COMMENT_TASK, StringUtils.ACTIVE_PARSE_TASK);
        commentObject.put(DatabaseUtils.COMMENT_SENDER, currentUser);
        commentObject.put(DatabaseUtils.COMMENT_RECEIVER, taskOwner);
        commentObject.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if(e == null){
                    Log.i("Parse", "Comment Saved On Parse");
                    startIntentService(message);
                    new SaveAcceptedTaskOnParse(true, tasksModel.getTaskVolunteerId()).execute();
                } else {
                    e.printStackTrace();
                }

            }
        });
    }

    private class SaveAcceptedTaskOnParse extends AsyncTask<String, Void, String> {

        private boolean isAccepted;
        private String taskVolunteerId;
        public SaveAcceptedTaskOnParse(boolean isAccepted, String taskVolunteerId){
            this.isAccepted = isAccepted;
            this.taskVolunteerId = taskVolunteerId;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if(!isAccepted)
                progressDialogManager.showDialog("Please Wait");
        }

        @Override
        protected String doInBackground(String... urls) {
            try {
                ParseQuery<ParseObject> query = ParseQuery.getQuery(DatabaseUtils.TABLE_TASKS_VOLUNTEERS);
                ParseObject parseObject = query.get(taskVolunteerId);
                if(isAccepted)
                    parseObject.put(DatabaseUtils.TASKS_VOLUNTEER_STATUS, TaskAssigningStatus.PENDING);
                else
                    parseObject.put(DatabaseUtils.TASKS_VOLUNTEER_STATUS, TaskAssigningStatus.REJECTED);
                parseObject.save();
            } catch (ParseException e){
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            progressDialogManager.dismissDialog();
        }
    }

    private void startIntentService(String message){
        try{
            ParseUser parseUser = ParseUser.getCurrentUser();
            ParseUser taskOwner = StringUtils.ACTIVE_PARSE_TASK.getParseUser(DatabaseUtils.TASKS_CREATED_BY);
            JSONObject object = new JSONObject();
            object.put("sender", parseUser.getObjectId());
            object.put("comment", message);
            object.put("commentTask", StringUtils.ACTIVE_PARSE_TASK.getObjectId());
            object.put("receiver", taskOwner.getObjectId());
            Intent intent = new Intent(context, PostCommentIntentService.class);
            intent.putExtra(StringUtils.COMMENT_JSON_OBJECT, object.toString());
            context.startService(intent);
        } catch (JSONException e){
            e.printStackTrace();
        }
    }

    private void scheduleReminder(Calendar calendar, int id, String message){
        Intent intent = new Intent(context, NotificationHandlerService.class);
        intent.putExtra(context.getString(R.string.notification_id_key), id);
        intent.putExtra(context.getString(R.string.notification_message_key), message);
        intent.putExtra(context.getString(R.string.notification_description_key), TaskNotificationDescription.NEW_ACTIVITY_ON_TASK);
        PendingIntent pendingIntent = PendingIntent.getService(context, id, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarm = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarm.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
    }

    /*private void cancelReminder(int id){
        Intent intent = new Intent(context, NotificationHandlerService.class);
        PendingIntent pendingIntent = PendingIntent.getService(context, id, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarm = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarm.cancel(pendingIntent);
        pendingIntent.cancel();
    }*/

    public void setAcceptanceButtonClickListener(AcceptanceButtonClickListener acceptanceButtonClickListener){
        this.acceptanceButtonClickListener = acceptanceButtonClickListener;
    }

    private class FetchOwnerPictureTask extends AsyncTask<String, Void, Bitmap> {

        private ImageView ivOwnerPicture;
        public FetchOwnerPictureTask(ImageView ivOwnerPicture){
            this.ivOwnerPicture = ivOwnerPicture;
        }

        @Override
        protected Bitmap doInBackground(String... urls) {
            Bitmap bitmap = null;
            try {
                String facebookId = urls[0];
                URL imageURL = new URL("https://graph.facebook.com/" + facebookId + "/picture?type=small");
                bitmap = BitmapFactory.decodeStream(imageURL.openConnection().getInputStream());
            } catch (MalformedURLException e){
                e.printStackTrace();
            } catch (IOException e){
                e.printStackTrace();
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            if (result != null)
                ivOwnerPicture.setImageBitmap(ImageUtils.cropToCircularImage(result));
        }
    }
}
