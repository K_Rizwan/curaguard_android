package com.curaguard.app.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.curaguard.app.R;
import com.curaguard.app.models.Comment;

import java.util.List;

/**
 * Created by Kashif on 5/7/2015.
 */
public class CommentsListAdapter extends BaseAdapter {

    private Context context;
    private List<Comment> commentsList;

    public CommentsListAdapter(Context context, List<Comment> items) {
        this.context = context;
        this.commentsList = items;
    }

    @Override
    public int getCount() {
        return commentsList.size();
    }

    @Override
    public Object getItem(int position) {
        return commentsList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return commentsList.indexOf(getItem(position));
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        final Comment comment = commentsList.get(position);

        LayoutInflater mInflater = (LayoutInflater)context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.comment_list_item, null);
            holder = new ViewHolder();
            holder.tvReceiver = (TextView) convertView.findViewById(R.id.textView_Receiver);
            holder.tvSender = (TextView) convertView.findViewById(R.id.textView_Sender);
            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        if(comment.isSender()){
            holder.tvReceiver.setVisibility(View.GONE);
            holder.tvSender.setVisibility(View.VISIBLE);

            holder.tvSender.setText(comment.getText());
        } else {
            holder.tvReceiver.setVisibility(View.VISIBLE);
            holder.tvSender.setVisibility(View.GONE);

            holder.tvReceiver.setText(comment.getText());
        }

        return convertView;
    }

    /*private view holder class*/
    private class ViewHolder {
        TextView tvReceiver;
        TextView tvSender;
    }
}
