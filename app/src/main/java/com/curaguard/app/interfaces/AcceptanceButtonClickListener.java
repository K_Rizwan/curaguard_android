package com.curaguard.app.interfaces;

/**
 * Created by Kashif on 5/9/2015.
 */
public interface AcceptanceButtonClickListener {
    public void onAcceptButtonClicked(int groupPosition, int childPosition);
}
