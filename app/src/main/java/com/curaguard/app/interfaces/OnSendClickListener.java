package com.curaguard.app.interfaces;

/**
 * Created by Kashif on 4/30/2015.
 */
public interface OnSendClickListener {
    public void onSendClicked(String message);
    public void onSetReminderClicked();
}
