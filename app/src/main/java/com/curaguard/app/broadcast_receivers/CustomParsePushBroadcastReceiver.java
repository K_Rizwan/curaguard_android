package com.curaguard.app.broadcast_receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.curaguard.app.R;
import com.curaguard.app.services.NotificationHandlerService;
import com.curaguard.app.utils.AlertsJsonManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

/**
 * Created by Kashif on 5/10/2015.
 */
public class CustomParsePushBroadcastReceiver extends BroadcastReceiver{

    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            Log.i("Broadcast Receiver", "Push Notification Received");
            JSONObject json = new JSONObject(intent.getExtras().getString("com.parse.Data"));
            Log.i("Broadcast Receiver Data", json.toString());

            String alert = json.getString("alert");
            String taskId = json.getString("taskId");
            String taskDescription = json.getString("taskDescription");
            AlertsJsonManager alertsJsonManager = new AlertsJsonManager(context);
            alertsJsonManager.addTaskIdToList(taskId);

            String message = alert + " (" + taskDescription + ")";
            int notificationId = (int)new Date().getTime();

            Intent notificationIntent = new Intent(context, NotificationHandlerService.class);
            notificationIntent.putExtra(context.getString(R.string.notification_id_key), notificationId);
            notificationIntent.putExtra(context.getString(R.string.notification_message_key), message);
            notificationIntent.putExtra(context.getString(R.string.notification_description_key), taskDescription);
            context.startService(notificationIntent);
        } catch (JSONException e){
            e.printStackTrace();
        }
    }
}
