package com.curaguard.app;

import android.app.Application;

import com.facebook.FacebookSdk;
import com.parse.Parse;
import com.parse.ParseInstallation;

/**
 * Created by Kashif on 4/22/2015.
 */
public class CuraGuardApplication extends Application {

    private static final String PARSE_APPLICATION_ID = "siOyh2x1La6fBTRyWXeTcfYVDEOjP58CWP7os8Iz";
    private static final String PARSE_CLIENT_KEY = "ysYl17EJyeJgtWhbJimEWm4vIbziUfnRvOeqNVaH";

    public static final String PARSE_PASSWORD = "curaguard123";

    @Override
    public void onCreate() {
        super.onCreate();

        FacebookSdk.sdkInitialize(this);
        Parse.enableLocalDatastore(getApplicationContext());
        Parse.initialize(getApplicationContext(), PARSE_APPLICATION_ID, PARSE_CLIENT_KEY);
        ParseInstallation.getCurrentInstallation().saveInBackground();
    }
}
