package com.curaguard.app.models;

import java.util.Date;

/**
 * Created by Kashif on 5/7/2015.
 */
public class Comment {
    private String text;
    private Date date;
    private boolean isSender;
    private String senderName;
    private String receiverName;

    public void setText(String text) {
        this.text = text;
    }

    public boolean isSender() {
        return isSender;
    }

    public void setSender(boolean isSender) {
        this.isSender = isSender;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    public String getText() {
        return text;
    }

    public Date getDate() {
        return date;
    }

    public String getSenderName() {
        return senderName;
    }

    public String getReceiverName() {
        return receiverName;
    }
}
