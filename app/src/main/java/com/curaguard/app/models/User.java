package com.curaguard.app.models;

/**
 * Created by Kashif on 5/5/2015.
 */
public class User {
    private String name;
    private String password;
    private String facebookId;
    private String gender;
    private String email;
    private String firstName;
    private String lastName;
    private String type;
    private String installationId;

    public void setName(String name) {
        this.name = name;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setFacebookId(String facebookId) {
        this.facebookId = facebookId;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setInstallationId(String installationId) {
        this.installationId = installationId;
    }

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

    public String getFacebookId() {
        return facebookId;
    }

    public String getGender() {
        return gender;
    }

    public String getEmail() {
        return email;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getType() {
        return type;
    }

    public String getInstallationId() {
        return installationId;
    }
}
