package com.curaguard.app.models;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Kashif on 4/28/2015.
 */
public class TasksModel implements Serializable {
    private String ownerName;
    private String ownerFBId;
    private String userId;
    private String taskId;
    private String taskVolunteerId;
    private String taskAssigningStatus;
    private String title;
    private int mutualFriends;
    private Date startDate;
    private String startTime;
    private String endTime;
    private boolean isAccepted;
    private String address;
    private String description;
    private float rating;

    public void setTaskAssigningStatus(String taskAssigningStatus) {
        this.taskAssigningStatus = taskAssigningStatus;
    }

    public String getTaskAssigningStatus() {
        return taskAssigningStatus;
    }

    public String getTaskVolunteerId() {
        return taskVolunteerId;
    }

    public void setTaskVolunteerId(String taskVolunteerId) {
        this.taskVolunteerId = taskVolunteerId;
    }

    public String getOwnerFBId() {
        return ownerFBId;
    }

    public void setOwnerFBId(String ownerFBId) {
        this.ownerFBId = ownerFBId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public void setMutualFriends(int mutualFriends) {
        this.mutualFriends = mutualFriends;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public void setAccepted(boolean isAccepted) {
        this.isAccepted = isAccepted;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public int getMutualFriends() {
        return mutualFriends;
    }

    public Date getStartDate() {
        return startDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public boolean isAccepted() {
        return isAccepted;
    }

    public String getAddress() {
        return address;
    }

    public String getDescription() {
        return description;
    }

    public float getRating() {
        return rating;
    }
}
