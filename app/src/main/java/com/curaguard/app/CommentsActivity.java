package com.curaguard.app;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.curaguard.app.R;
import com.curaguard.app.adapters.CommentsListAdapter;
import com.curaguard.app.models.Comment;
import com.curaguard.app.services.PostCommentIntentService;
import com.curaguard.app.utils.DatabaseUtils;
import com.curaguard.app.utils.KeyboardUtils;
import com.curaguard.app.utils.PreferenceManager;
import com.curaguard.app.utils.ProgressDialogManager;
import com.curaguard.app.utils.StringUtils;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class CommentsActivity extends Activity {

    private TextView tvHeaderTitle, tvHeaderLogOut, tvHeaderRefresh;
    private ImageView ivHeaderPlusButton;
    private ListView listViewComments;
    private Button btnSendComment;
    private EditText etComment;
    private List<Comment> comments;
    private CommentsListAdapter commentsListAdapter;
    private PreferenceManager preferenceManager;
    private ProgressDialogManager progressDialogManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comments);
        progressDialogManager = new ProgressDialogManager(this);
        preferenceManager = new PreferenceManager(this);
        mHandler = new Handler();

        initControls();
        tvHeaderTitle.setText(getResources().getString(R.string.comments));
        tvHeaderLogOut.setText("Back");
        tvHeaderLogOut.setVisibility(View.VISIBLE);
        tvHeaderRefresh.setVisibility(View.GONE);
        ivHeaderPlusButton.setVisibility(View.GONE);
        tvHeaderLogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btnSendComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(etComment.getText().toString().equals("")){
                    Toast.makeText(CommentsActivity.this, "Please Enter Comment First", Toast.LENGTH_SHORT).show();
                } else {
                    Comment comment = new Comment();
                    comment.setText(etComment.getText().toString());
                    comment.setSender(true);
                    comments.add(comment);
                    commentsListAdapter = new CommentsListAdapter(CommentsActivity.this, comments);
                    listViewComments.setAdapter(commentsListAdapter);
                    saveCommentOnParse(etComment.getText().toString());
                    etComment.setText("");
                    KeyboardUtils.hideKeyboard(CommentsActivity.this);
                }
            }
        });

        new FetchCommentsTasks(false).execute("");
    }

    private int mInterval = 10000;
    private Handler mHandler;
    private boolean flag = false;
    Runnable mStatusChecker = new Runnable() {
        @Override
        public void run() {
            if (flag)
                new FetchCommentsTasks(true).execute("");
            flag = true;
            mHandler.postDelayed(mStatusChecker, mInterval);
        }
    };

    void startRepeatingTask() {
        mStatusChecker.run();
    }

    void stopRepeatingTask() {
        mHandler.removeCallbacks(mStatusChecker);
    }

    @Override
    protected void onResume() {
        super.onResume();
        startRepeatingTask();
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopRepeatingTask();
    }

    private void startIntentService(String message){
        try{
            ParseUser parseUser = ParseUser.getCurrentUser();
            ParseUser taskOwner = StringUtils.ACTIVE_PARSE_TASK.getParseUser(DatabaseUtils.TASKS_CREATED_BY);
            JSONObject object = new JSONObject();
            object.put("sender", parseUser.getObjectId());
            object.put("comment", message);
            object.put("commentTask", StringUtils.ACTIVE_PARSE_TASK.getObjectId());
            object.put("receiver", taskOwner.getObjectId());
            Intent intent = new Intent(CommentsActivity.this, PostCommentIntentService.class);
            intent.putExtra(StringUtils.COMMENT_JSON_OBJECT, object.toString());
            startService(intent);
        } catch (JSONException e){
            e.printStackTrace();
        }
    }

    private void saveCommentOnParse(final String message){
        ParseUser taskOwner = StringUtils.ACTIVE_PARSE_TASK.getParseUser(DatabaseUtils.TASKS_CREATED_BY);
        ParseObject commentParseObject = new ParseObject(DatabaseUtils.TABLE_COMMENT);
        commentParseObject.put(DatabaseUtils.COMMENT_MESSAGE, message);
        commentParseObject.put(DatabaseUtils.COMMENT_SENDER, ParseUser.getCurrentUser());
        commentParseObject.put(DatabaseUtils.COMMENT_RECEIVER, taskOwner);
        commentParseObject.put(DatabaseUtils.COMMENT_TASK, StringUtils.ACTIVE_PARSE_TASK);
        commentParseObject.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                Log.i("Parse", "Comment Saved On Parse");
                startIntentService(message);
            }
        });
    }

    private void initControls() {
        tvHeaderTitle = (TextView) findViewById(R.id.textView_headerTitle);
        tvHeaderLogOut = (TextView) findViewById(R.id.textView_logoutButton);
        tvHeaderRefresh = (TextView) findViewById(R.id.textView_refreshButton);
        etComment = (EditText) findViewById(R.id.editText_Comment);
        ivHeaderPlusButton = (ImageView) findViewById(R.id.imageView_PlusButton);
        btnSendComment = (Button) findViewById(R.id.button_SendComment);
        listViewComments = (ListView)findViewById(R.id.listViewComments);
    }

    private class FetchCommentsTasks extends AsyncTask<String, Void, List<ParseObject>> {
        ParseUser parseUser;
        Boolean isUpdateTask;
        public FetchCommentsTasks(Boolean isUpdateTask){
            this.isUpdateTask = isUpdateTask;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            comments = new ArrayList<Comment>();
            if(!isUpdateTask)
                progressDialogManager.showDialog("Please Wait");
        }

        @Override
        protected List<ParseObject> doInBackground(String... urls) {
            try {
                parseUser = ParseUser.getCurrentUser();

                ParseQuery<ParseObject> receiverQuery = ParseQuery.getQuery(DatabaseUtils.TABLE_COMMENT);
                receiverQuery.whereEqualTo(DatabaseUtils.COMMENT_RECEIVER, parseUser);

                ParseQuery<ParseObject> senderQuery = ParseQuery.getQuery(DatabaseUtils.TABLE_COMMENT);
                senderQuery.whereEqualTo(DatabaseUtils.COMMENT_SENDER, parseUser);

                List<ParseQuery<ParseObject>> queries = new ArrayList<ParseQuery<ParseObject>>();
                queries.add(receiverQuery);
                queries.add(senderQuery);

                ParseQuery<ParseObject> mainQuery = ParseQuery.or(queries);
                mainQuery.whereEqualTo(DatabaseUtils.COMMENT_TASK, StringUtils.ACTIVE_PARSE_TASK);
                mainQuery.include(DatabaseUtils.COMMENT_TASK);
                mainQuery.include(DatabaseUtils.COMMENT_SENDER);
                mainQuery.include(DatabaseUtils.COMMENT_RECEIVER);
                List<ParseObject> commentObjects = mainQuery.find();
                Log.d("Comments", commentObjects.toString());
                return commentObjects;
            } catch (ParseException e){
                e.printStackTrace();
            }
            return new ArrayList<ParseObject>();
        }

        @Override
        protected void onPostExecute(List<ParseObject> commentObjects) {
            for (ParseObject commentObject : commentObjects){
                Comment comment = new Comment();
                comment.setText(commentObject.getString(DatabaseUtils.COMMENT_MESSAGE));
                ParseUser sender = commentObject.getParseUser(DatabaseUtils.COMMENT_SENDER);
                if (sender != null){
                    if(sender.getObjectId().equals(parseUser.getObjectId())){
                        comment.setSender(true);
                    } else{
                        comment.setSender(false);
                    }
                    comment.setReceiverName("");
                    comment.setSenderName("");
                    comments.add(comment);
                }
            }
            commentsListAdapter = new CommentsListAdapter(CommentsActivity.this, comments);
            listViewComments.setAdapter(commentsListAdapter);
            if(!isUpdateTask)
                progressDialogManager.dismissDialog();

        }
    }
}
