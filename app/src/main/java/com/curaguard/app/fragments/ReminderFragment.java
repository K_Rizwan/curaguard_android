package com.curaguard.app.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.curaguard.app.R;
import com.curaguard.app.adapters.RemindersListAdapter;
import com.curaguard.app.models.TasksModel;
import com.curaguard.app.utils.RemindersJsonManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kashif on 4/8/2015.
 */
public class ReminderFragment extends Fragment {

    private ListView listViewReminder;
    private RemindersJsonManager remindersJsonManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.reminder_fragment, container, false);
        listViewReminder = (ListView)view.findViewById(R.id.listView_Reminders);
        remindersJsonManager = new RemindersJsonManager(getActivity());

        listViewReminder.setAdapter(new RemindersListAdapter(getActivity(), populateDataSet()));

        return view;
    }

    private List<TasksModel> populateDataSet(){
        List<TasksModel> dataSet = new ArrayList<TasksModel>();
        JSONArray remindersJsonArray = remindersJsonManager.fetchAllReminders();
        if(remindersJsonArray != null && remindersJsonArray.length() > 0){
            Log.i("Reminders", remindersJsonArray.toString());
            for (int i = 0; i < remindersJsonArray.length(); i++){
                try {
                    JSONObject object = remindersJsonArray.getJSONObject(i);
                    dataSet.add(remindersJsonManager.convertJsonToTaskModel(object));
                } catch (JSONException e){

                }
            }
        }
        return dataSet;
    }
}
