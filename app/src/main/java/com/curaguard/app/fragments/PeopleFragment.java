package com.curaguard.app.fragments;

import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.curaguard.app.R;
import com.curaguard.app.adapters.PeopleListAdapter;
import com.curaguard.app.utils.ProgressDialogManager;
import com.curaguard.app.utils.ToastManager;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseRelation;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kashif on 4/8/2015.
 */
public class PeopleFragment extends Fragment {

    private ListView listViewPeople;
    private ProgressDialogManager progressDialogManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.people_fragment, container, false);
        listViewPeople = (ListView)view.findViewById(R.id.listView_People);
        progressDialogManager = new ProgressDialogManager(getActivity());
        new FetchUserFriends().execute("");
        return view;
    }

    private class FetchUserFriends extends AsyncTask<String, Void, List<ParseUser>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialogManager.showDialog("Please Wait");
        }

        @Override
        protected List<ParseUser> doInBackground(String... urls) {
            try {
                ParseUser user = ParseUser.getCurrentUser();
                ParseRelation<ParseUser> relation = user.getRelation("friends");
                ParseQuery<ParseUser> query = relation.getQuery();
                List<ParseUser> userFriends = query.find();
                Log.i("Friends", "" + userFriends.toString());
                return userFriends;
            } catch (ParseException e){
                e.printStackTrace();
                ToastManager.ShowToast(getActivity(), e.getMessage());
            }
            return new ArrayList<ParseUser>();
        }

        @Override
        protected void onPostExecute(List<ParseUser> result) {
            progressDialogManager.dismissDialog();
            if (result != null)
                listViewPeople.setAdapter(new PeopleListAdapter(getActivity(), result));
        }
    }
}
