package com.curaguard.app.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.curaguard.app.CommentsActivity;
import com.curaguard.app.R;
import com.curaguard.app.adapters.ExpandableListAdapter;
import com.curaguard.app.interfaces.AcceptanceButtonClickListener;
import com.curaguard.app.models.TasksModel;
import com.curaguard.app.utils.AlertsJsonManager;
import com.curaguard.app.utils.DatabaseUtils;
import com.curaguard.app.utils.ImageUtils;
import com.curaguard.app.utils.PreferenceManager;
import com.curaguard.app.utils.ProgressDialogManager;
import com.curaguard.app.utils.StringUtils;
import com.curaguard.app.utils.TaskAssigningStatus;
import com.curaguard.app.utils.TaskStatus;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Kashif on 4/8/2015.
 */
public class HomeFragment extends Fragment{

    private ExpandableListAdapter listAdapter;
    private ExpandableListView expListView;
    private List<String> listDataHeader;
    private ImageView ivUserPhoto;
    private ProgressBar progressBar;
    private TextView tvName, tvActiveTasks, tvAvailableTasks, tvCompletedTasks;
    private HashMap<String, List<TasksModel>> listDataChild;
    private PreferenceManager preferenceManager;
    private ProgressDialogManager progressDialogManager;
    private List<ParseObject> activeParseObjects;
    private List<ParseObject> availableParseObjects;
    private List<ParseObject> completedParseObjects;

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.home_fragment, container, false);
        preferenceManager = new PreferenceManager(getActivity());
        progressDialogManager = new ProgressDialogManager(getActivity());

        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        expListView = (ExpandableListView) view.findViewById(R.id.expandableListView);
        ivUserPhoto = (ImageView) view.findViewById(R.id.imageView_userImage);
        tvActiveTasks = (TextView) view.findViewById(R.id.textView_activeTasks);
        tvName = (TextView) view.findViewById(R.id.textView_userName);
        tvAvailableTasks = (TextView) view.findViewById(R.id.textView_AvailableTasks);
        tvCompletedTasks = (TextView) view.findViewById(R.id.textView_CompletedTasks);

        String userName = preferenceManager.getUserName(PreferenceManager.USER_NAME);
        String Url = preferenceManager.getUserPicturePath(PreferenceManager.USER_PICTURE_PATH);
        Log.i("PictureUrl", Url);

        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                if(groupPosition != 2){
                    final AlertsJsonManager alertsJsonManager = new AlertsJsonManager(getActivity());
                    switch (groupPosition){
                        case 0:
                            StringUtils.ACTIVE_PARSE_TASK = activeParseObjects.get(childPosition);
                            break;
                        case 1:
                            StringUtils.ACTIVE_PARSE_TASK = availableParseObjects.get(childPosition);
                            break;
                        case 2:
                            StringUtils.ACTIVE_PARSE_TASK = completedParseObjects.get(childPosition);
                            break;
                        default:
                            break;
                    }
                    Thread thread = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            alertsJsonManager.deleteTaskIds(StringUtils.ACTIVE_PARSE_TASK.getObjectId());
                        }
                    });
                    thread.start();
                    Intent intent = new Intent(getActivity(), CommentsActivity.class);
                    startActivity(intent);
                }
                return false;
            }
        });

        tvName.setText(userName);
        new DisplayUserPictureTask().execute(Url);

        prepareListData();

        return view;
    }

    public void refreshAllTasks(){
        Log.i("Refresh Tasks", "Refresh Tasks Clicked");
        expListView.collapseGroup(0);
        expListView.collapseGroup(1);
        expListView.collapseGroup(2);
        progressBar.setVisibility(View.VISIBLE);
        prepareListData();
    }

    private class DisplayUserPictureTask extends AsyncTask<String, Void, Bitmap> {
        @Override
        protected Bitmap doInBackground(String... urls) {
            File image = new File(Environment.getExternalStorageDirectory() + "/CuraGuard", StringUtils.USER_PROFILE_PICTURE_NAME);
            Bitmap bitmap = BitmapFactory.decodeFile(image.getAbsolutePath());
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            if (result != null)
                ivUserPhoto.setImageBitmap(ImageUtils.cropToCircularImage(result));
        }
    }

    private void prepareListData() {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<TasksModel>>();

        // Adding child data
        listDataHeader.add(getResources().getString(R.string.active_jobs));
        listDataHeader.add(getResources().getString(R.string.available_jobs));
        listDataHeader.add(getResources().getString(R.string.completed_jobs));

        List<TasksModel> tasksModels = new ArrayList<TasksModel>();
        listDataChild.put(listDataHeader.get(0), tasksModels);
        listDataChild.put(listDataHeader.get(1), tasksModels);
        listDataChild.put(listDataHeader.get(2), tasksModels);

        new FetchFriendTasks().execute("");
        //prepareActiveTasksData();
    }

    private class FetchFriendTasks extends AsyncTask<String, Void, String> {

        List<TasksModel> activeTasksModels;
        List<TasksModel> availableTasksModels;
        List<TasksModel> completedTasksModels;

        public FetchFriendTasks(){
            activeTasksModels = new ArrayList<TasksModel>();
            availableTasksModels = new ArrayList<TasksModel>();
            completedTasksModels = new ArrayList<TasksModel>();

            activeParseObjects = new ArrayList<ParseObject>();
            availableParseObjects = new ArrayList<ParseObject>();
            completedParseObjects = new ArrayList<ParseObject>();
        }

        @Override
        protected String doInBackground(String... urls) {
            try {
                ParseUser currentUser = ParseUser.getCurrentUser();
                ParseQuery<ParseObject> tasksVolunteerQuery = ParseQuery.getQuery(DatabaseUtils.TABLE_TASKS_VOLUNTEERS);
                tasksVolunteerQuery.whereEqualTo(DatabaseUtils.TASKS_VOLUNTEER_USER, currentUser);
                tasksVolunteerQuery.include(DatabaseUtils.TASKS_VOLUNTEER_USER);
                tasksVolunteerQuery.include(DatabaseUtils.TASKS_VOLUNTEER_OWNER);
                tasksVolunteerQuery.include(DatabaseUtils.TASKS_VOLUNTEER_TASK);
                List<ParseObject> taskVolunteersList = tasksVolunteerQuery.find();
                Log.v("TaskVolunteers", taskVolunteersList.toString());
                for (ParseObject taskVolunteerObject : taskVolunteersList){
                    ParseObject taskObject = taskVolunteerObject.getParseObject(DatabaseUtils.TASKS_VOLUNTEER_TASK);
                    ParseUser parseUser = taskVolunteerObject.getParseUser(DatabaseUtils.TASKS_VOLUNTEER_OWNER);
                    String status = taskVolunteerObject.getString(DatabaseUtils.TASKS_VOLUNTEER_TASK_STATUS);
                    if(!taskVolunteerObject.getString(DatabaseUtils.TASKS_VOLUNTEER_STATUS).equals(TaskAssigningStatus.REJECTED)){
                        if(status.equalsIgnoreCase(TaskStatus.ACTIVE)){
                            activeParseObjects.add(taskObject);
                            TasksModel activeTask = new TasksModel();
                            if(parseUser != null){
                                activeTask.setOwnerName(parseUser.getString(DatabaseUtils.Users_NAME));
                                activeTask.setOwnerFBId(parseUser.getString(DatabaseUtils.Users_FBID));
                                activeTask.setUserId(parseUser.getObjectId());
                            }
                            activeTask.setTaskVolunteerId(taskVolunteerObject.getObjectId());
                            activeTask.setTaskId(taskObject.getObjectId());
                            activeTask.setTaskAssigningStatus(taskVolunteerObject.getString(DatabaseUtils.TASKS_VOLUNTEER_STATUS));
                            activeTask.setMutualFriends(5);
                            activeTask.setTitle(taskObject.getString(DatabaseUtils.TASKS_TITLE));
                            activeTask.setStartDate(taskObject.getDate(DatabaseUtils.TASKS_START_DATE));
                            activeTask.setStartTime(taskObject.getString(DatabaseUtils.TASKS_START_TIME));
                            activeTasksModels.add(activeTask);
                        } else if(status.equalsIgnoreCase(TaskStatus.OPEN)){
                            availableParseObjects.add(taskObject);
                            TasksModel availableTask = new TasksModel();
                            if(parseUser != null){
                                availableTask.setOwnerName(parseUser.getString(DatabaseUtils.Users_NAME));
                                availableTask.setOwnerFBId(parseUser.getString(DatabaseUtils.Users_FBID));
                                availableTask.setUserId(parseUser.getObjectId());
                            }
                            availableTask.setTaskVolunteerId(taskVolunteerObject.getObjectId());
                            availableTask.setTaskId(taskObject.getObjectId());
                            availableTask.setTaskAssigningStatus(taskVolunteerObject.getString(DatabaseUtils.TASKS_VOLUNTEER_STATUS));
                            availableTask.setMutualFriends(5);
                            availableTask.setTitle(taskObject.getString(DatabaseUtils.TASKS_TITLE));
                            availableTask.setStartDate(taskObject.getDate(DatabaseUtils.TASKS_START_DATE));
                            availableTask.setStartTime(taskObject.getString(DatabaseUtils.TASKS_START_TIME));
                            availableTasksModels.add(availableTask);
                        } else if (status.equalsIgnoreCase(TaskStatus.CLOSE)){
                            completedParseObjects.add(taskObject);
                            TasksModel completedTask = new TasksModel();
                            if(parseUser != null){
                                completedTask.setOwnerName(parseUser.getString(DatabaseUtils.Users_NAME));
                                completedTask.setOwnerFBId(parseUser.getString(DatabaseUtils.Users_FBID));
                                completedTask.setUserId(parseUser.getObjectId());
                            }
                            completedTask.setTaskVolunteerId(taskVolunteerObject.getObjectId());
                            completedTask.setTaskId(taskObject.getObjectId());
                            completedTask.setTaskAssigningStatus(taskVolunteerObject.getString(DatabaseUtils.TASKS_VOLUNTEER_STATUS));
                            completedTask.setMutualFriends(5);
                            completedTask.setStartDate(taskObject.getDate(DatabaseUtils.TASKS_START_DATE));

                            ParseQuery<ParseObject> queryRatings = ParseQuery.getQuery(DatabaseUtils.TABLE_RATING);
                            queryRatings.whereEqualTo(DatabaseUtils.RATING_TASK_ID, taskObject);
                            queryRatings.include(DatabaseUtils.RATING_TASK_ID);
                            List<ParseObject> ratingObjects = queryRatings.find();
                            if(ratingObjects.size() > 0){
                                ParseObject ratingObject = ratingObjects.get(0);
                                completedTask.setRating(ratingObject.getInt(DatabaseUtils.RATING_RATING));
                            } else{
                                completedTask.setRating(0);
                            }
                            completedTasksModels.add(completedTask);
                        }
                    }
                }

            } catch (ParseException e){
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            tvActiveTasks.setText("Active Tasks: " + activeTasksModels.size());
            tvAvailableTasks.setText("Available Tasks: " + availableTasksModels.size());
            tvCompletedTasks.setText("Completed Tasks: " + completedTasksModels.size());

            listDataChild.put(listDataHeader.get(0), activeTasksModels);
            listDataChild.put(listDataHeader.get(1), availableTasksModels);
            listDataChild.put(listDataHeader.get(2), completedTasksModels);

            progressBar.setVisibility(View.GONE);

            listAdapter = new ExpandableListAdapter(getActivity(), listDataHeader, listDataChild);
            listAdapter.setAcceptanceButtonClickListener(new AcceptanceButtonClickListener() {
                @Override
                public void onAcceptButtonClicked(int groupPosition, int childPosition) {
                    switch (groupPosition){
                        case 0:
                            StringUtils.ACTIVE_PARSE_TASK = activeParseObjects.get(childPosition);
                            break;
                        case 1:
                            StringUtils.ACTIVE_PARSE_TASK = availableParseObjects.get(childPosition);
                            break;
                        case 2:
                            StringUtils.ACTIVE_PARSE_TASK = completedParseObjects.get(childPosition);
                            break;
                        default:
                            break;
                    }
                }
            });
            expListView.setAdapter(listAdapter);
        }
    }
}